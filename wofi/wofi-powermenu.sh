#!/bin/bash

entries=" Reboot\n Shutdown"

selected=$(echo -e $entries | wofi --width 280 --height 170 -p '' --dmenu --cache-file /dev/null | awk '{print tolower($2)}')

case $selected in
  reboot)
    exec systemctl reboot;;
  shutdown)
    exec systemctl poweroff -i;;
esac